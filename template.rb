require 'bitbucket_rest_api'

def base_path(path)
  "/tmp/app-generator/files/#{path}"
end

def copy_file_from_base(file)
  copy_file base_path(file), file
end

def copy_dir_from_base(dir)
  directory base_path(dir), dir
end

def add_and_commit(msg)
  git add: '.'
  git commit: "-a -m '#{msg}'"
end

run "sudo rm -r /tmp/app-generator"
run "cd /tmp && git clone git@bitbucket.org:tracersoft/app-generator.git"

bitbucket = BitBucket.new do |config|
  config.basic_auth = ENV['BITBUCKET_BASIC_AUTH']
end

#########################

copy_file_from_base '.gitignore'

git :init
git add: "."
git commit: "-a -m 'Initial commit'"

#########################

run "heroku apps:create #{app_name}"

gem 'pg'
gem 'bootstrap-sass', '~> 3.2.0'
gem 'font-awesome-sass', '~> 4.2.0'
gem 'select2-rails', '~> 3.5.9.1'
gem 'devise'
gem 'quiet_assets'
gem 'jquery-turbolinks'
gem 'kaminari'
gem 'pundit'
gem 'rspec'

gem_group :production do
  gem 'newrelic_rpm'
end

gem_group :staging, :production do
  gem 'unicorn'
  gem 'unicorn-rails'
  gem 'rails_12factor'
end

gem_group :development, :test do
  gem 'dotenv-rails'
  gem 'pry'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'awesome_print'
  gem 'factory_girl'
  gem 'factory_girl_rails'
end

gem_group :test do
  gem 'capybara'
  gem 'database_cleaner'
  gem 'rspec-rails'
  gem 'mocha'
  gem 'poltergeist'
end

run 'bundle install'

add_and_commit '-a -m "Bundle install"'

#########################

generate('pundit:install')
add_and_commit '-a -m "Pundit install"'

#########################

copy_file_from_base 'config/database.yml'

add_and_commit '-a -m "Configure database"'

#########################

copy_file_from_base 'config/environments/staging.rb'

mailer_config = 'config.action_mailer.default_url_options = { host: "localhost", port: 3000 }'
environment mailer_config, env: 'test'
environment mailer_config, env: 'development'
environment "##{mailer_config}", env: 'production'
environment "##{mailer_config}", env: 'development'

add_and_commit '-a -m "Configure environments"'

#########################

generate('devise:install')
generate(:devise, 'User')

add_and_commit '-a -m "Devise install"'

#########################

generate('rspec:install')

copy_file_from_base 'spec/rails_helper.rb'
copy_dir_from_base 'spec/support'

add_and_commit '-a -m "RSpec install"'

#########################

file 'README.md', <<-CODE
#{app_name}
===========

```
run rake db:setup
copy .env.sample -> .env
```
CODE

add_and_commit '-a -m " Generate README.md"'

#########################

file '.env.sample', <<-CODE
DATABASE_URL=postgres://user:password@localhost:5432/#{app_name}
TEST_DATABASE_URL=postgres://user:password@localhost:5432/#{app_name}_test
CODE

add_and_commit '-a -m "Generate .env.sample"'

#########################

copy_file_from_base 'config/unicorn.rb'

add_and_commit '-a -m "Generate config/unicorn.rb"'

#########################

copy_file_from_base 'Procfile'

add_and_commit '-a -m "Generate Procfile"'

#########################

run 'git clone git@bitbucket.org:tracersoft/ansible.git'
add_and_commit '-a -m "Download ansible resources"'

#########################

copy_file_from_base 'Vagrantfile'

add_and_commit '-a -m "Generate Vagrantfile"'

#########################

unless bitbucket.repos.list.map(&:slug).include? app_name
  bitbucket.repos.create("name"=>app_name,
    "description"=>"Auto generated repo for #{app_name}",
    "is_private"=>true,
    "has_issues"=>true,
    "has_wiki"=>true,
    "owner"=>'tracersoft')
end

git remote: "add origin git@bitbucket.org:tracersoft/#{app_name}"
git push: "--set-upstream origin master"
